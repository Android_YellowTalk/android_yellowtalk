package com.ayapa.yellowtalk;

/**
 * Created by User on 7/23/2016.
 */
public class YellowTalkConstant {

    public static final String BASE_URL = "http://ayapaapi.citrustelecom.net";
    public static final int HTTP_CONNECT_TIMEOUT = 3000; // milliseconds
    public static final int HTTP_READ_TIMEOUT = 3000; // milliseconds
}
