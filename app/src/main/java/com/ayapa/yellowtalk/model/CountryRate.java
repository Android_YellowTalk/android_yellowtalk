package com.ayapa.yellowtalk.model;

/**
 * Created by Thanjeedth on 8/15/2016.
 */
public class CountryRate {
    public String name;
    public String code;
    public String rateMobile;
    public String rateLandLine;
    public String isoCode;
    public int imageID;

    public CountryRate(String name, String code, String rateMobile, String rateLandLine, String isoCode, int resourceId) {
        this.name = name;
        this.code = code;
        this.rateMobile = rateMobile;
        this.rateLandLine = rateLandLine;
        this.isoCode = isoCode;
        this.imageID = resourceId;
    }

}
