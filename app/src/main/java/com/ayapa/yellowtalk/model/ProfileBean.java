package com.ayapa.yellowtalk.model;

/**
 * Created by Thanjeedth on 8/29/2016.
 */

public class ProfileBean {
    String name;
    String phone;
    String email;
    String sex;
    String dateCreated;
    String city;
    String country;

    public ProfileBean() {
    }

    public ProfileBean(String name, String phone, String email, String sex, String dateCreated, String country, String city) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.sex = sex;
        this.dateCreated = dateCreated;
        this.country = country;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
