package com.ayapa.yellowtalk.model;

/**
 * Created by Thanjeedth on 8/19/2016.
 */
public class CallLogBean {
    public int id;
    public String name;
    public String number;
    public String date;
    public String type;
    public String count;

    public CallLogBean(String name, String number, String date, String type, String count) {
        this.name = name;
        this.number = number;
        this.date = date;
        this.type = type;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
