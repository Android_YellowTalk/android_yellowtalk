package com.ayapa.yellowtalk.model;

public class ContactBean {
    //private int contactId;
    public String name;
    public String phoneNo;

    public ContactBean(String name, String phoneNo) {
        this.name = name;
        this.phoneNo = phoneNo;
    }
    /*private Long photoId;
    private String formattedNumber;*/

    /*public int getContactId() { return contactId; }
    public void setContactId(int contactId) { this.contactId = contactId; }*/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
    /*public Long getPhotoId() { return photoId; }
    public void setPhotoId(Long photoId) { this.photoId = photoId; }
	public String getFormattedNumber() { return formattedNumber; }
	public void setFormattedNumber(String formattedNumber) { this.formattedNumber = formattedNumber; }*/
}
