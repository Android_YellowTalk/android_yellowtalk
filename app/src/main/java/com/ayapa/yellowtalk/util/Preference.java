package com.ayapa.yellowtalk.util;

import android.app.Activity;
import android.content.SharedPreferences;

public class Preference {

    public static void savePreference(String key, String value, Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String showPreference(String key, Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        String savedPref = sharedPreferences.getString(key, "");
        return savedPref;
    }

    /*public static void savePreferenceLoginStatus(String key, String value, Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String showPreferenceLoginStatus(String key, Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        String savedPref = sharedPreferences.getString(key, "");
        return savedPref;
    }

    public static void savePreferenceRokaId(String key, String value, Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String showPreferenceRokaId(String key, Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        String savedPref = sharedPreferences.getString(key, "");
        return savedPref;
    }

    public static void savePreferenceMobileNo(String key, String value, Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String showPreferenceMobileNo(String key, Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        String savedPref = sharedPreferences.getString(key, "");
        return savedPref;
    }

    public static void savePreferenceLoginToken(String key, String value, Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String showPreferenceLoginToken(String key, Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(key, Activity.MODE_PRIVATE);
        String savedPref = sharedPreferences.getString(key, "");
        return savedPref;
    }*/

}
