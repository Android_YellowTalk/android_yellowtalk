package com.ayapa.yellowtalk.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    public static String getDateFromMiliseconds(String miliseconds) {
        String date = null;

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        long milliSeconds = Long.parseLong(miliseconds);
        System.out.println(milliSeconds);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        date = formatter.format(calendar.getTime());

        return date;
    }

    public static String getMilisecondsFromDate() {
        long milliSeconds = new Date().getTime();

        return String.valueOf(milliSeconds);
    }

    public static String getCurrentDate() {
        String date = null;

        DateFormat formatter = new SimpleDateFormat("dd/MM/yy");

        date = formatter.format(new Date());

        return date;
    }

}
