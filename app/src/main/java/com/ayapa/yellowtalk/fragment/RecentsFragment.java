package com.ayapa.yellowtalk.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.activity.ContactDetailsActivity;
import com.ayapa.yellowtalk.adapter.RecentListViewAdapter;
import com.ayapa.yellowtalk.model.ContactBean;
import com.ayapa.yellowtalk.model.CountryRate;
import com.ayapa.yellowtalk.util.NetworkStatus;

import java.sql.Date;
import java.util.ArrayList;

/**
 * Created by Thanjeedth on 8/10/2016.
 */
public class RecentsFragment extends Fragment {

    ListView lview;
    RecentListViewAdapter lviewAdapter;
    TextView tvPhone;

    private int recentCount;

    public int getRecentCount() {
        return recentCount;
    }

    public void setRecentCount(int recentCount) {
        this.recentCount = recentCount;
    }



    String[] phoneArr, dateArr, durationArr;

    ArrayList<ContactBean> recentCallList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recents_layout, container, false);

        boolean isNetworkConnect = NetworkStatus.isInternetConnected(getActivity());
        if (!isNetworkConnect) {
            NetworkStatus.popupDataWifiEnableMessage(getActivity());
        } else {
        }

        getCallDetails();


        tvPhone = (TextView) view.findViewById(R.id.tvPhoneNo);

        lview = (ListView) view.findViewById(R.id.lvRecent);
        lviewAdapter = new RecentListViewAdapter(getActivity(), phoneArr, dateArr, durationArr);

        if (lviewAdapter != null){
            lview.setAdapter(lviewAdapter);

            lview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    if(phoneArr[position] != null){
                        Log.i("Clicked Item1 >> ", phoneArr[position].toString());
                        Log.i("Clicked Prefix >> ", phoneArr[position].toString().substring(0,1));
                        String takePhone = (phoneArr[position].toString().substring(0,1).equalsIgnoreCase("+") ? phoneArr[position].toString().substring(3) : phoneArr[position].toString().substring(4));
                        Log.i("Clicked Item number >> ", takePhone);
                        String phone = "tel:+443309980120,," + takePhone;
                        Log.i("Clicked Item2 >> ", phone);

                    /*Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse(phone));
                    startActivity(i);*/
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse(phone));
                        intent.putExtra("incoming_number", phone);
                        //getContext().startActivity(intent);
                        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        getContext().startActivity(intent);

                    } else {
                        Log.i("Clicked Item1 >> ", "null");
                    }
                }
            });

        }
        return view;
    }

    private void getCallDetails() {

        StringBuffer sb = new StringBuffer();
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Cursor managedCursor = getActivity().getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls.DATE + " DESC limit 10;"); //DESC limit 1
        this.setRecentCount(managedCursor.getCount());
        Log.e("getColumnCount() >> ", getRecentCount() + "");

        if(this.getRecentCount()>= 10){
            phoneArr = new String[10];
            dateArr = new String[10];
            durationArr = new String[10];
        } else {
            phoneArr = new String[this.getRecentCount()];
            dateArr = new String[this.getRecentCount()];
            durationArr = new String[this.getRecentCount()];
        }

        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        sb.append("Call Details :");
        int count = 0;

        recentCallList = new ArrayList<ContactBean>();

        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            ContactBean cb = new ContactBean("", phNumber);
            recentCallList.add(cb);

//            phoneArr[count] = "Phone Number : \t" + phNumber;
//            dateArr[count] = "Date : \t\t\t\t\t" + callDayTime + "";
//            durationArr[count] = "Duration : \t\t\t" + callDuration + " sec";
            phoneArr[count] = phNumber;
            dateArr[count] = callDayTime + "";
            durationArr[count] = callDuration + " sec";
            count++;
        }
        managedCursor.close();
        //call.setText(sb);
    }
}
