package com.ayapa.yellowtalk.fragment;

/**
 * Created by Thanjeedth on 8/10/2016.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.util.NetworkStatus;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;


public class AccountFragment extends Fragment {

    private static PayPalConfiguration config = new PayPalConfiguration().environment(PayPalConfiguration.ENVIRONMENT_NO_NETWORK).clientId("<YOUR_CLIENT_ID>");
    //Intent nextIntent;
    Button addCredit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.account_layout, container, false);

        boolean isNetworkConnect = NetworkStatus.isInternetConnected(getActivity());
        if (!isNetworkConnect) {
            NetworkStatus.popupDataWifiEnableMessage(getActivity());
        } else {
        }

        addCredit = (Button) view.findViewById(R.id.btnAddCredit);

        addCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] listPayMethods = {"Voucher", "Pay Pal", "Credit Card"};
                String[] listPayAmounts = {"£ 2", "£ 5", "£ 7"};
                ArrayAdapter<String> adapterPayMethod = new ArrayAdapter<String>(getActivity(), R.layout.payment_method_popup_itm, listPayMethods);
                final ArrayAdapter<String> adapterPayAmount = new ArrayAdapter<String>(getActivity(), R.layout.payment_method_popup_itm, listPayAmounts);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setAdapter(adapterPayMethod, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Log.e("Payment Method : ", "Voucher");
                                dialog.dismiss();
                                break;
                            case 1:
                                dialog.dismiss();
                                Log.e("Payment Method : ", "Pay Pal");
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setAdapter(adapterPayAmount, new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        switch (which) {
                                            case 0:
                                                Log.e("Amount : ", "£ 2");
                                                break;
                                            case 1:
                                                Log.e("Amount : ", "£ 5");
                                                break;
                                            case 2:
                                                Log.e("Amount : ", "£ 7");
                                                break;
                                        }
                                        Intent intent = new Intent(getActivity(), PayPalService.class);
                                        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
                                        getActivity().startService(intent);
                                        onBuyPressed(adapterPayAmount.getItem(which).substring(2, 3));

                                    }

                                });

                                TextView myTitle = new TextView(getActivity());
                                myTitle.setText("Top-up Yellow Talk Account By");
                                myTitle.setTextSize(24);
                                myTitle.setTypeface(null, Typeface.BOLD);
                                myTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                                builder.setCustomTitle(myTitle);

                                AlertDialog alert = builder.show();
                                ListView listView = alert.getListView();
                                listView.setDivider(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
                                listView.setDividerHeight(2);
                                Window window = alert.getWindow();
                                WindowManager.LayoutParams wlp = window.getAttributes();

                                wlp.gravity = Gravity.CENTER;
                                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                                window.setAttributes(wlp);
                                break;
                            case 2:
                                dialog.dismiss();
                                Log.e("Payment Method : ", "Credit Card");
                                break;
                            /*case 3:
                                dialog.dismiss();

                                break;*/
                        }
                    }
                });

                TextView myTitle = new TextView(getActivity());
                myTitle.setText("Payment Method\n" +
                        "Select payment type");
                myTitle.setTextSize(24);
                myTitle.setTypeface(null, Typeface.BOLD);
                myTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                builder.setCustomTitle(myTitle);

                AlertDialog alert = builder.show();

                ListView listView = alert.getListView();
                listView.setDivider(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
                listView.setDividerHeight(2);

                Window window = alert.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();

                wlp.gravity = Gravity.BOTTOM;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);

            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i("paymentExample", confirm.toJSONObject().toString(4));

                    // TODO: send 'confirm' to your server for verification.
                    // see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                    // for more details.

                } catch (JSONException e) {
                    Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.");
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }
    }

    @Override
    public void onDestroy() {
        getActivity().stopService(new Intent(getActivity(), PayPalService.class));
        super.onDestroy();
    }

    public void onBuyPressed(String amount) {

        // PAYMENT_INTENT_SALE will cause the payment to complete immediately.
        // Change PAYMENT_INTENT_SALE to
        //   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
        //   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
        //     later via calls from your server.

        PayPalPayment payment = new PayPalPayment(new BigDecimal(amount), "GBP", "Yellow Talk Top-up",
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(getActivity(), PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        startActivityForResult(intent, 0);
    }
}