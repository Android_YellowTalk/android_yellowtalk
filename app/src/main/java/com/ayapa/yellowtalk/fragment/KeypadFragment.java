package com.ayapa.yellowtalk.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.util.NetworkStatus;

/**
 * Created by Thanjeedth on 8/10/2016.
 */
public class KeypadFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //startActivity(new Intent(Intent.ACTION_DIAL, null));

        View view = inflater.inflate(R.layout.keypad_layout, null);

        boolean isNetworkConnect = NetworkStatus.isInternetConnected(getActivity());
        if (!isNetworkConnect) {
            NetworkStatus.popupDataWifiEnableMessage(getActivity());
        } else {
        }

        return view;
    }

}
