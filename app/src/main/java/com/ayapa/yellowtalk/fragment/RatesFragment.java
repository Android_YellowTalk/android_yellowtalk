package com.ayapa.yellowtalk.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.activity.RateDetailsActivity;
import com.ayapa.yellowtalk.adapter.ListViewAdapter;
import com.ayapa.yellowtalk.model.CountryRate;
import com.ayapa.yellowtalk.util.JSONParser;
import com.ayapa.yellowtalk.util.NetworkStatus;
import com.ayapa.yellowtalk.util.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class RatesFragment extends Fragment implements SearchView.OnQueryTextListener {

    //JSON Node Names
    private static final String TAG_COUNTRY_RATES = "country_rates";
    private static final String TAG_COUNTRY_NAME = "country_name";
    private static final String TAG_COUNTRY_CODE = "country_code";
    private static final String TAG_ISO_CODE = "iso_code";
    private static final String TAG_MOBILE_RATE = "mobile";
    private static final String TAG_LANDLINE_RATE = "landline";

    ListView list;
    ImageView ivFlag;
    TextView tvCountry, tvCode, tvRate;
    SearchView searview;

    Context context = getActivity();

    JSONArray rates = null;

    ArrayList<CountryRate> countryRateList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.rates_layout, container, false);

        boolean isNetworkConnect = NetworkStatus.isInternetConnected(getActivity());
        if (!isNetworkConnect) {
            NetworkStatus.popupDataWifiEnableMessage(getActivity());
        } else {
        }

        ivFlag = (ImageView) v.findViewById(R.id.ivFlag);
        tvCountry = (TextView) v.findViewById(R.id.tvCountry);
        tvCode = (TextView) v.findViewById(R.id.tvCode);
        searview = (SearchView) v.findViewById(R.id.searchViewBar);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //countrylist = new ArrayList<HashMap<String, String>>();

        new JSONParse().execute();
    }

    private void setupSearchView() {
        searview.setIconifiedByDefault(false);
        searview.setOnQueryTextListener(this);
        searview.setSubmitButtonEnabled(true);
        searview.setQueryHint("Search Here");
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        if (TextUtils.isEmpty(newText)) {
            list.clearTextFilter();
        } else {
            list.setFilterText(newText);
        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("country_rates.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private class JSONParse extends AsyncTask<String, String, JSONObject> {

        JSONParser jParser = new JSONParser();
        private ProgressDialog pDialog;
        private JSONObject json;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading Country Rates ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }

        @Override
        protected JSONObject doInBackground(String... args) {

            JSONParser jParser = new JSONParser();
            // Getting JSON from string
            try {
                json = new JSONObject(loadJSONFromAsset());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {

            pDialog.dismiss();
            try {

                // Getting JSON Array from URL
                rates = json.getJSONArray("country_rates");
                Log.i("Json ARRAY", rates.toString());
                countryRateList = new ArrayList<CountryRate>();
                // adapter = new CountryRateAdapter(getActivity(), countryRateList);

//                country = new String[rates.length()];
//                code = new String[rates.length()];
//                flag = new int[rates.length()];

                for (int i = 0; i < rates.length(); i++) {

                    JSONObject c = rates.getJSONObject(i);

                    //country[i] = c.getString(TAG_COUNTRY_NAME);
                    // code[i] = c.getString(TAG_COUNTRY_CODE);

                    Resources resources = getActivity().getResources();
                    int resourceId = resources.getIdentifier(c.getString(TAG_ISO_CODE).toLowerCase(), "drawable",
                            getActivity().getPackageName());
                    //flag[i] = resourceId;

                    // Storing  JSON item in a Variable
                    String imageCode = c.getString(TAG_ISO_CODE);
                    String name = c.getString(TAG_COUNTRY_NAME);
                    String code = c.getString(TAG_COUNTRY_CODE);
                    String rateLand = c.getString(TAG_LANDLINE_RATE);
                    String rateMobile = c.getString(TAG_MOBILE_RATE);

                    // Adding value HashMap key => value
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_ISO_CODE, imageCode.toLowerCase());
                    map.put(TAG_COUNTRY_NAME, name);
                    map.put(TAG_COUNTRY_CODE, code);
                    map.put(TAG_LANDLINE_RATE, rateLand);
                    map.put(TAG_MOBILE_RATE, rateMobile);

                    //countrylist.add(map);
                    CountryRate cr = new CountryRate(name, code, rateMobile, rateLand, imageCode.toLowerCase(), resourceId);
                    countryRateList.add(cr);

                }

                list = (ListView) getView().findViewById(android.R.id.list);
                list.setAdapter(new ListViewAdapter(getActivity(), countryRateList));
                list.setTextFilterEnabled(true);

                setupSearchView();

                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        CountryRate model = (CountryRate) list.getAdapter().getItem(position);
                        Log.e("test", "" + model.isoCode);
                        Intent intent = new Intent(getActivity().getBaseContext(),
                                RateDetailsActivity.class);
                        intent.putExtra("imageCode", model.isoCode);
                        intent.putExtra("name", model.name);
                        intent.putExtra("code", model.code);
                        intent.putExtra("rateLand", model.rateLandLine);
                        intent.putExtra("rateMobile", model.rateMobile);
                        getActivity().startActivity(intent);

                        Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "4", getActivity());

                        // Toast.makeText(getActivity(), countrylist.get(+position).get(TAG_COUNTRY_NAME), Toast.LENGTH_SHORT).show();
                    }
                });

//
//                searview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//
//                    @Override
//                    public boolean onQueryTextSubmit(String query) {
//                        //list.clearTextFilter();
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onQueryTextChange(String newText) {
//                        //adapter.getFilter().filter(newText);
//                        // new ItemFilter().performFiltering(newText);
//                        if (newText.length() > 0) {
//                            list.setFilterText(newText.toString());
//                              list.fil
//                            //new ItemFilter().performFiltering(newText);
//                            //list.setFilterText(newText.toString());
//                            //list.refreshDrawableState();
//                        } else
//                            list.clearTextFilter();
//
//                        return true;
//                    }
//                });


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}