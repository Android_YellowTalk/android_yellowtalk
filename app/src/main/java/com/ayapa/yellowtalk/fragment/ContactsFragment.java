package com.ayapa.yellowtalk.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.activity.ContactDetailsActivity;
import com.ayapa.yellowtalk.activity.VerifyCodeActivity;
import com.ayapa.yellowtalk.model.ContactBean;
import com.ayapa.yellowtalk.util.NetworkStatus;
import com.ayapa.yellowtalk.util.StringMatcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class ContactsFragment extends Fragment implements SearchView.OnQueryTextListener {

    private static CustomAdapter adapter;

    String[] mProjection;
    ListView listView;
    Map<String, Integer> mapIndex;

    SearchView searchText;

    final private int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 123;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contacts_layout, container, false);

        boolean isNetworkConnect = NetworkStatus.isInternetConnected(getActivity());
        if (!isNetworkConnect) {
            NetworkStatus.popupDataWifiEnableMessage(getActivity());
        } else {
        }

        try {

            mProjection = new String[]{
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER
            };

            final Cursor cursor = getActivity().getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    mProjection,
                    null,
                    null,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
            );

            //  contactNames = new String[cursor.getCount()];

            //  Log.i("Contacts length >>", String.valueOf(contactNames.length));
            adapter = new CustomAdapter(getActivity(), cursor);

            // Create the list view and bind the adapter
            listView = (ListView) view.findViewById(android.R.id.list);
            listView.setAdapter(adapter);
            listView.setTextFilterEnabled(true);
            searchText = (SearchView) view.findViewById(R.id.inputSearch);
            setupSearchView();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Cursor cursor = adapter.getCursor();
                    cursor.moveToPosition(position);

                    ContactBean model = new ContactBean(cursor.getString(0), cursor.getString(1));

                    Intent myIntent = new Intent(getActivity(), ContactDetailsActivity.class);
                    Log.i("Clicked Item >> ", model.getName() + " " + model.getPhoneNo());
                    myIntent.putExtra("selected_name", model.getName());
                    myIntent.putExtra("selected_phone_number", model.getPhoneNo());
                    getActivity().startActivity(myIntent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            insertDummyContactWrapper();

        }

        return view;
    }

    private void insertDummyContactWrapper() {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[] {Manifest.permission.READ_CONTACTS},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            return;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS:
            {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_CONTACTS, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    //insertDummyContact();
                } else {
                    // Permission Denied
                    Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void getIndexList(String[] contacts) {
        mapIndex = new LinkedHashMap<String, Integer>();
        for (int i = 0; i < contacts.length; i++) {
            String contact = contacts[i];
            String index = contact.substring(0, 1);

            if (mapIndex.get(index) == null)
                mapIndex.put(index, i);
        }
    }

    private void setupSearchView() {
        searchText.setIconifiedByDefault(false);
        searchText.setOnQueryTextListener(this);
        searchText.setSubmitButtonEnabled(true);
        searchText.setQueryHint("Search Here");
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            listView.clearTextFilter();
        } else {
            listView.setFilterText(newText);
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public class CustomAdapter extends BaseAdapter implements Filterable, SectionIndexer {
        // State of the row that needs to show separator
        private static final int SECTIONED_STATE = 1;
        // State of the row that need not show separator
        private static final int REGULAR_STATE = 2;
        List<String> searchArrayName;
        private Context mContext;
        private Cursor mCursor;
        // Cache row states based on positions
        private int[] mRowStates;

        private String mSections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public CustomAdapter(Context context, Cursor cursor) {
            mContext = context;
            mCursor = cursor;
            mRowStates = new int[getCount()];
        }

        // Filter Class

        @Override
        public int getCount() {

            return mCursor.getCount();
        }

        public Cursor getCursor() {
            return mCursor;
        }

        @Override
        public Object getItem(int position) {
//            ContactBean contactBean = new ContactBean();
//            contactBean.setName(mCursor.getString( mCursor.getColumnIndex(mProjection[0]) ));
//            contactBean.setPhoneNo(mCursor.getString( mCursor.getColumnIndex(mProjection[1]) ));1
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view;
            boolean showSeparator = false;

            mCursor.moveToPosition(position);

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.contact_list_item, null);
            } else {
                view = convertView;
            }

            // Set contact name and number
            TextView contactNameView = (TextView) view.findViewById(R.id.contact_name);
            TextView phoneNumberView = (TextView) view.findViewById(R.id.phone_number);
            String name = mCursor.getString(mCursor.getColumnIndex(mProjection[0]));
            String number = mCursor.getString(mCursor.getColumnIndex(mProjection[1]));

            contactNameView.setText(name);
            phoneNumberView.setText(number);

            switch (mRowStates[position]) {

                case SECTIONED_STATE:
                    showSeparator = true;
                    break;

                case REGULAR_STATE:
                    //contactNames[position]= name;
                    showSeparator = false;
                    break;

                default:

                    if (position == 0) {
                        showSeparator = true;
                    } else {
                        mCursor.moveToPosition(position - 1);

                        String previousName = mCursor.getString(mCursor.getColumnIndex(mProjection[0]));
                        char[] previousNameArray = previousName.toCharArray();
                        char[] nameArray = name.toCharArray();

                        if (nameArray[0] != previousNameArray[0]) {
                            showSeparator = true;
                        }

                        mCursor.moveToPosition(position);
                    }

                    // Cache it
                    mRowStates[position] = showSeparator ? SECTIONED_STATE : REGULAR_STATE;

                    break;
            }

            TextView separatorView = (TextView) view.findViewById(R.id.separator);

            if (showSeparator) {
                separatorView.setText(name.toCharArray(), 0, 1);
                separatorView.setVisibility(View.VISIBLE);
            } else {
                view.findViewById(R.id.separator).setVisibility(View.GONE);
            }

            return view;
        }

        public Filter getFilter() {
            return new Filter() {

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    constraint = constraint.toString().toLowerCase();
                    FilterResults result = new FilterResults();
                    Log.e("ADDED SEARCH", constraint + "");

                    ContentResolver cr = getActivity().getContentResolver();
                    if (constraint.length() > 0) {
                        mCursor = cr.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                mProjection,
                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " like ?",
                                new String[]{constraint + "%"},
                                null);

                    } else {
                        mCursor = getActivity().getContentResolver().query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                mProjection,
                                null,
                                null,
                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
                        );
                    }
                    return result;


                }

                @Override
                protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
                    //final ArrayList<CountryRate> filteredDate = (ArrayList<CountryRate>) results.values;
                    notifyDataSetChanged();
                }

            };
        }

        @Override
        public int getPositionForSection(int section) {
            // If there is no item for current section, previous section will be selected
            for (int i = section; i >= 0; i--) {
                for (int j = 0; j < getCount(); j++) {
                    if (i == 0) {
                        // For numeric section
                        for (int k = 0; k <= 9; k++) {
                            if (StringMatcher.match(String.valueOf(mSections.charAt(0)), String.valueOf(k)))
                                return j;
                        }
                    } else {
                        if (StringMatcher.match(String.valueOf(mSections.charAt(0)), String.valueOf(mSections.charAt(i))))
                            return j;
                    }
                }
            }
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            return 0;
        }

        @Override
        public Object[] getSections() {
            String[] sections = new String[mSections.length()];
            for (int i = 0; i < mSections.length(); i++)
                sections[i] = String.valueOf(mSections.charAt(i));
            return sections;
        }

    }
}