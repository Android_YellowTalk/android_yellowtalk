package com.ayapa.yellowtalk.db;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

import com.ayapa.yellowtalk.model.ProfileBean;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "yellowtalk.db";
    public static final String CALL_TABLE_NAME = "call_info";
    public static final String CALL_COLUMN_ID = "id";
    public static final String CALL_COLUMN_FROM = "phone_no_from";
    public static final String CALL_COLUMN_TO = "phone_no_to";
    public static final String CALL_COLUMN_START = "call_start_time";
    public static final String CALL_COLUMN_END = "call_end_time";
    public static final String CALL_COLUMN_DURATION = "duration";

    public static final String PROFILE_TABLE_NAME = "profile";
    public static final String PROFILE_COLUMN_ID = "id";
    public static final String PROFILE_COLUMN_NAME = "name";
    public static final String PROFILE_COLUMN_PHONE = "phone_no";
    public static final String PROFILE_COLUMN_EMAIL = "email";
    public static final String PROFILE_COLUMN_SEX = "sex";
    public static final String PROFILE_COLUMN_CREATED_DATE = "created_date";
    public static final String PROFILE_COLUMN_CITY = "city";
    public static final String PROFILE_COLUMN_COUNTRY = "country";
    public static final String PROFILE_COLUMN_IMAGE = "image";

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table " + CALL_TABLE_NAME +
                        "(" + CALL_COLUMN_ID + " integer primary key, " + CALL_COLUMN_FROM + " text, "
                        + CALL_COLUMN_TO + " text, " + CALL_COLUMN_START + " text, "
                        + CALL_COLUMN_END + " text, " + CALL_COLUMN_DURATION + " text)");

        db.execSQL(
                "create table " + PROFILE_TABLE_NAME +
                        "(" + PROFILE_COLUMN_ID + " integer primary key, " + PROFILE_COLUMN_NAME + " text, "
                        + PROFILE_COLUMN_PHONE + " text, " + PROFILE_COLUMN_EMAIL + " text, "
                        + PROFILE_COLUMN_SEX + " text, " + PROFILE_COLUMN_CREATED_DATE + " text,"
                        + PROFILE_COLUMN_CITY + " text," + PROFILE_COLUMN_COUNTRY + " text)");
                        /*+ PROFILE_COLUMN_CITY + " text," + PROFILE_COLUMN_COUNTRY + " text," + PROFILE_COLUMN_IMAGE + " BLOB)");*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CALL_TABLE_NAME);
        onCreate(db);
    }

    public boolean insertCallInfo(String callFromNo, String callToNo, String callStartTime, String callEndTime,String duration)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CALL_COLUMN_FROM, callFromNo);
        contentValues.put(CALL_COLUMN_TO, callToNo);
        contentValues.put(CALL_COLUMN_START, callStartTime);
        contentValues.put(CALL_COLUMN_END, callEndTime);
        contentValues.put(CALL_COLUMN_DURATION, duration);
        db.insert(CALL_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertProfileInfo(ProfileBean profileBean)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PROFILE_COLUMN_NAME, profileBean.getName());
        contentValues.put(PROFILE_COLUMN_PHONE, profileBean.getPhone());
        contentValues.put(PROFILE_COLUMN_EMAIL, profileBean.getEmail());
        contentValues.put(PROFILE_COLUMN_SEX, profileBean.getSex());
        contentValues.put(PROFILE_COLUMN_CREATED_DATE, profileBean.getDateCreated());
        contentValues.put(PROFILE_COLUMN_CITY, profileBean.getCity());
        contentValues.put(PROFILE_COLUMN_COUNTRY, profileBean.getCountry());
        //contentValues.put(PROFILE_COLUMN_IMAGE, image);
        db.insert(PROFILE_TABLE_NAME, null, contentValues);

        /*int id = (int) db.insertWithOnConflict("PROFILE_TABLE_NAME", null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        if (id == -1) {
            db.update("PROFILE_TABLE_NAME", contentValues, "phone_no=?", new String[] {Integer.toString(id)});  // number 1 is the _id here, update to variable for your code
        }*/

        //db.close(); // Closing database connection
        return true;
    }

    public Cursor getCallData(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from " + CALL_TABLE_NAME + " where id="+id+"", null );
        return res;
    }

    public ProfileBean getProfileData(String name){
        ProfileBean profileBean = new ProfileBean();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res=db.query(PROFILE_TABLE_NAME ,null,  "name=?",new String[]{name},null, null, null, null);

        if(res.getCount()<1){
            res.close();
            return null;
        }
        else if(res.getCount()>=1 && res.moveToLast()){

            profileBean.setName(res.getString(res.getColumnIndex(PROFILE_COLUMN_NAME)));
            profileBean.setPhone(res.getString(res.getColumnIndex(PROFILE_COLUMN_PHONE)));
            profileBean.setEmail(res.getString(res.getColumnIndex(PROFILE_COLUMN_EMAIL)));
            profileBean.setDateCreated(res.getString(res.getColumnIndex(PROFILE_COLUMN_CREATED_DATE)));
            profileBean.setSex(res.getString(res.getColumnIndex(PROFILE_COLUMN_SEX)));
            profileBean.setCity(res.getString(res.getColumnIndex(PROFILE_COLUMN_CITY)));
            profileBean.setCountry(res.getString(res.getColumnIndex(PROFILE_COLUMN_COUNTRY)));
            res.close();
            return profileBean;

        }

        return null;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, CALL_TABLE_NAME);
        return numRows;
    }

    public boolean updateRecentCall(Integer id, String callFromNo, String callToNo, String callStartTime, String callEndTime,String duration)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("call_from_no", callFromNo);
        contentValues.put("call_to_no", callToNo);
        contentValues.put("call_start_time", callStartTime);
        contentValues.put("call_end_time", callEndTime);
        contentValues.put("duration", duration);
        db.update(CALL_TABLE_NAME, contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Integer deleteRecentCallFromLog (Integer id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(CALL_TABLE_NAME,
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

    public ArrayList<String> getAllRecentCalls()
    {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from " + CALL_TABLE_NAME, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex(CALL_COLUMN_ID)));
            res.moveToNext();
        }
        return array_list;
    }
}
