package com.ayapa.yellowtalk.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.util.NetworkStatus;

public class RateDetailsActivity extends AppCompatActivity {

    ImageView flag;
    TextView name, code, rateLand, rateMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        boolean isNetworkConnect = NetworkStatus.isInternetConnected(this);
        if (!isNetworkConnect) {
            NetworkStatus.popupDataWifiEnableMessage(this);
        } else {
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        flag = (ImageView) findViewById(R.id.flag);
        name = (TextView) findViewById(R.id.name);
        code = (TextView) findViewById(R.id.code);
        rateLand = (TextView) findViewById(R.id.rateLand);
        rateMobile = (TextView) findViewById(R.id.rateMobile);

        Log.i("ImageCode >> ", intent.getStringExtra("imageCode"));
        Resources res = getResources();
        int resourceId = res.getIdentifier(
                intent.getStringExtra("imageCode"), "drawable", getPackageName());
        flag.setImageResource(resourceId);
        name.setText(intent.getStringExtra("name"));
        code.setText(intent.getStringExtra("code"));
        rateLand.setText(intent.getStringExtra("rateLand"));
        rateMobile.setText(intent.getStringExtra("rateMobile"));

    }

}
