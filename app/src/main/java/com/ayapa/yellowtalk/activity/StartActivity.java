package com.ayapa.yellowtalk.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.service.model.Request.AccountRequest;
import com.ayapa.yellowtalk.util.NetworkStatus;
import com.ayapa.yellowtalk.util.Preference;

import rx.Subscription;

public class StartActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String PREFS_ACC_NO = "ACCOUNT_NO";
    Button buttonContinue;
    TextView terms;
    boolean isNetworkConnect;
    Intent myIntent;
    private AccountRequest mAccountRequest;
    private Subscription mSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonContinue = (Button) findViewById(R.id.btn_continue);
        buttonContinue.setOnClickListener(this);

        terms = (TextView)findViewById(R.id.link_terms);
        terms.setOnClickListener(this);

        isNetworkConnect = NetworkStatus.isInternetConnected(this);
        if (!isNetworkConnect) {
            NetworkStatus.popupDataWifiEnableMessage(this);
        } else {
        }
        //TO DO
        //ButterKnife.bind(this);

        //load popup notifications message 1st time load the app
        String popupNotifyStatus = Preference.showPreference(getString(R.string.sharedPrefNotificationPopup), StartActivity.this);
        if (popupNotifyStatus.equalsIgnoreCase("1")) {

        } else if (popupNotifyStatus == null){
            popupNOtificationMessage();
        } else {
            popupNOtificationMessage();
        }

    }

    @Override
    protected void onDestroy() {
        if (mSubscription != null)
            this.mSubscription.unsubscribe();

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_continue:
                if (!isNetworkConnect) {
                    NetworkStatus.popupDataWifiEnableMessage(this);
                } else {
                    myIntent = new Intent(StartActivity.this, RegisterActivity.class);
                    startActivity(myIntent);

                }
                break;
            case R.id.link_terms:
                if (!isNetworkConnect) {
                    NetworkStatus.popupDataWifiEnableMessage(this);
                } else {
                    myIntent = new Intent(StartActivity.this, TermsAndConditionsActivity.class);
                    startActivity(myIntent);
                    //finish();
                }
            default:
                break;

        }
    }

    private void popupNOtificationMessage() {
        // popup message allow notifications
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                //.setTitle("Yello Talk Would Like to Send You Notifications")
                //.setMessage("Notifications may include alerts, sounds and icon badges. These can be configured in Settings.")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Preference.savePreference(getString(R.string.sharedPrefNotificationPopup), "1", StartActivity.this);
                        // Allow notifications
                        // push notifications on

                    }
                })
                .setNegativeButton("Don't Allow", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Preference.savePreference(getString(R.string.sharedPrefNotificationPopup), "1", StartActivity.this);
                        // Restrict notifications
                        // push notifications off

                    }
                });

        TextView myTitle = new TextView(this);
        myTitle.setText("Yello Talk Would Like to Send You Notifications");
        myTitle.setTextSize(18);
        myTitle.setTypeface(null, Typeface.BOLD);
        myTitle.setGravity(Gravity.CENTER_HORIZONTAL);
        builder.setCustomTitle(myTitle);

        TextView myMsg = new TextView(this);
        myMsg.setText("Notifications may include alerts, sounds and icon badges. These can be configured in Settings.");
        myMsg.setTextSize(18);
        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        builder.setView(myMsg);

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

        wmlp.gravity = Gravity.CENTER;
        wmlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;

        dialog.show();

    }

}
