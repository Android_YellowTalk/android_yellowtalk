package com.ayapa.yellowtalk.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.util.Preference;

public class SplashAvtivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2500;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash);

        String loginStatus = Preference.showPreference(getString(R.string.sharedPrefLoginStatus), SplashAvtivity.this);
        //loginStatus = "LOGOUT";
        Log.e("SP loginStatus : ", loginStatus);
        if (loginStatus.equalsIgnoreCase("LOGIN")) {
            intent = new Intent(SplashAvtivity.this, MainActivity.class);
        } /*else if (loginStatus.equalsIgnoreCase("LOGOUT")) {
            intent = new Intent(SplashAvtivity.this, YellowTalkLoginActivity.class);
        } */else {
            Log.e("SP loginStatus : ", "NULL");
            intent = new Intent(SplashAvtivity.this, StartActivity.class);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
