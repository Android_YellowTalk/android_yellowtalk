package com.ayapa.yellowtalk.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.db.DBHelper;
import com.ayapa.yellowtalk.model.ProfileBean;
import com.ayapa.yellowtalk.util.DateUtils;
import com.ayapa.yellowtalk.util.NetworkStatus;

import java.io.FileDescriptor;
import java.io.IOException;

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private static int RESULT_LOAD_IMAGE = 1;
    ImageView imageView;

    EditText etName, etPhone, etEmail, etSex, etDateCreated, etCity, etCountry;
    Button save;
    DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        db = new DBHelper(MyProfileActivity.this);
        imageView = (ImageView) findViewById(R.id.ivProfilePic);
        imageView.setOnClickListener(this);

        etName= (EditText)findViewById(R.id.etName);
        etPhone =(EditText)findViewById(R.id.etPhone);
        etEmail=(EditText)findViewById(R.id.etEmail);
        etSex =(EditText)findViewById(R.id.etSex);
        etDateCreated=(EditText)findViewById(R.id.etProfileCreated);
        etCity=(EditText)findViewById(R.id.etCity);
        etCountry=(EditText)findViewById(R.id.etCountry);

        ProfileBean profileBean = db.getProfileData("mihira");
        if (profileBean != null) {
            etName.setText(profileBean.getName());
            etPhone.setText(profileBean.getPhone());
            etEmail.setText(profileBean.getEmail());
            etSex.setText(profileBean.getSex());
            etDateCreated.setText(profileBean.getDateCreated());
            etCity.setText(profileBean.getCity());
            etCountry.setText(profileBean.getCountry());
        } else {
            etDateCreated.setText(DateUtils.getCurrentDate());

        }


        save = (Button)findViewById(R.id.btn_save_profile);
        save.setOnClickListener(MyProfileActivity.this);

        boolean isNetworkConnect = NetworkStatus.isInternetConnected(this);
        if(!isNetworkConnect) {
            NetworkStatus.popupDataWifiEnableMessage(this);
        } else {
            /*Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_IMAGE);*/
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();



            Bitmap bmp = null;
            try {
                bmp = getBitmapFromUri(selectedImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
            imageView.setImageBitmap(bmp);

        }


    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivProfilePic:
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
                break;
            case R.id.btn_save_profile:
                String name = etName.getText().toString();
                String phone = etPhone.getText().toString();
                String email = etEmail.getText().toString();
                String sex = etSex.getText().toString();
                String dateCreated = etDateCreated.getText().toString();
                String city = etCity.getText().toString();
                String country = etCountry.getText().toString();

                ProfileBean profile = new ProfileBean();

                profile.setName(name);
                profile.setPhone(phone);
                profile.setEmail(email);
                profile.setSex(sex);
                profile.setDateCreated(dateCreated);
                profile.setCity(city);
                profile.setCountry(country);
                db = new DBHelper(MyProfileActivity.this);
                boolean profileSuccess = db.insertProfileInfo(profile);
                if(profileSuccess)
                    Toast.makeText(MyProfileActivity.this, "Success", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(MyProfileActivity.this, "Fail", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
