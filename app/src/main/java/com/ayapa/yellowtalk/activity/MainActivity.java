package com.ayapa.yellowtalk.activity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.fragment.AccountFragment;
import com.ayapa.yellowtalk.fragment.ContactsFragment;
import com.ayapa.yellowtalk.fragment.KeypadFragment;
import com.ayapa.yellowtalk.fragment.RatesFragment;
import com.ayapa.yellowtalk.fragment.RecentsFragment;
import com.ayapa.yellowtalk.fragment.TabFragment;
import com.ayapa.yellowtalk.util.Preference;
//import com.google.firebase.auth.FirebaseAuth;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    String lastTab;
    private BottomBar mBottomBar;
    private Intent nextIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_navi);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // create sqlite database to store call information
        SQLiteDatabase mydatabase = openOrCreateDatabase("yellowtalk_db", MODE_PRIVATE, null);
        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS yellowtalk_call_info(Username VARCHAR,Password VARCHAR);");
        Log.e("yellowtalk_call_info", "yellowtalk_call_info table created");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerTabView, new TabFragment()).commit();

        // Bottom Bar
        mBottomBar = BottomBar.attach(this, savedInstanceState);
        mBottomBar.setItems(R.menu.bottombar_menu);
        mBottomBar.setOnMenuTabClickListener(new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.bottomBarItemContacts) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerTabView, new ContactsFragment()).commit();
                    Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "1", MainActivity.this);
                } else if (menuItemId == R.id.bottomBarItemRecents) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerTabView, new RecentsFragment()).commit();
                    Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "2", MainActivity.this);
                } else if (menuItemId == R.id.bottomBarItemKeypad) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerTabView, new KeypadFragment()).commit();
                    Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "3", MainActivity.this);
                } else if (menuItemId == R.id.bottomBarItemRates) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerTabView, new RatesFragment()).commit();
                    Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "4", MainActivity.this);
                } else if (menuItemId == R.id.bottomBarItemAccount) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerTabView, new AccountFragment()).commit();
                    Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "5", MainActivity.this);
                }

            }

            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.bottomBarItemContacts) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerTabView, new ContactsFragment()).commit();
                    Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "1", MainActivity.this);
                } else if (menuItemId == R.id.bottomBarItemRecents) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerTabView, new RecentsFragment()).commit();
                    Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "2", MainActivity.this);
                } else if (menuItemId == R.id.bottomBarItemKeypad) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerTabView, new KeypadFragment()).commit();
                    Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "3", MainActivity.this);
                } else if (menuItemId == R.id.bottomBarItemRates) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerTabView, new RatesFragment()).commit();
                    Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "4", MainActivity.this);
                } else if (menuItemId == R.id.bottomBarItemAccount) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerTabView, new AccountFragment()).commit();
                    Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "5", MainActivity.this);
                }
            }
        });

        // Setting colors for different tabs when there's more than three of them.
        // You can set colors for tabs in three different ways as shown below.

        /*mBottomBar.mapColorForTab(0, ContextCompat.getColor(this, R.color.colorPrimary));
        mBottomBar.mapColorForTab(1, ContextCompat.getColor(this, R.color.colorPrimary));
        mBottomBar.mapColorForTab(2, ContextCompat.getColor(this, R.color.colorPrimary));
        mBottomBar.mapColorForTab(3, ContextCompat.getColor(this, R.color.colorPrimary));
        mBottomBar.mapColorForTab(4, ContextCompat.getColor(this, R.color.colorPrimary));*/

        /*lastTab = Preference.showPreference(getResources().getString(R.string.sharedPrefLastTabOpened), MainActivity.this);
        Log.e("Last Tab", lastTab);
        String value = lastTab;
        if ("1".equals(value )) {
            Log.e("1 Tab", lastTab);
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.containerTabView,new RecentsFragment()).commit();
            Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "1", MainActivity.this);
        }
        else if ("2".equals(value )) {
            Log.e("2 Tab", lastTab);
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.containerTabView,new ContactsFragment()).commit();
            Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "2", MainActivity.this);
        }
        else if ("3".equals(value )) {
            Log.e("3 Tab", lastTab);
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.containerTabView,new KeypadFragment()).commit();
            Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "3", MainActivity.this);
        }
        else if ("4".equals(value )) {
            Log.e("4 Tab", lastTab);
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.containerTabView,new RatesFragment()).commit();
            Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "4", MainActivity.this);
        }
        else if ("5".equals(value )) {
            Log.e("5 Tab", lastTab);
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.containerTabView,new AccountFragment()).commit();
            Preference.savePreference(getResources().getString(R.string.sharedPrefLastTabOpened), "5", MainActivity.this);
        }*/
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_calltype) {
            nextIntent = new Intent(MainActivity.this, CallTypeActivity.class);
            startActivity(nextIntent);
        } else if (id == R.id.nav_chat) {
            nextIntent = new Intent(MainActivity.this, ChatActivity.class);
            startActivity(nextIntent);
        } else if (id == R.id.nav_invite) {
            nextIntent = new Intent(MainActivity.this, InviteFriendsActivity.class);
            startActivity(nextIntent);
        } else if (id == R.id.nav_help) {
            nextIntent = new Intent(MainActivity.this, HelpSupportActivity.class);
            startActivity(nextIntent);
        } else if (id == R.id.nav_aboutus) {
            nextIntent = new Intent(MainActivity.this, AboutUsActivity.class);
            startActivity(nextIntent);
        } else if (id == R.id.nav_mydetails) {
            nextIntent = new Intent(MainActivity.this, MyProfileActivity.class);
            startActivity(nextIntent);
        } else if (id == R.id.nav_signout) {
            /*Preference.savePreference(getString(R.string.sharedPrefLoginStatus), "LOGOUT", MainActivity.this);
            finish();
            //FirebaseAuth.getInstance().signOut();
            nextIntent = new Intent(MainActivity.this, YellowTalkLoginActivity.class);
            startActivity(nextIntent);*/
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
