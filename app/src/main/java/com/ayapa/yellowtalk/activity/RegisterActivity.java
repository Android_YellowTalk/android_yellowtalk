package com.ayapa.yellowtalk.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.service.YellowTalkService;
import com.ayapa.yellowtalk.service.model.Request.SubscriberRequest;
import com.ayapa.yellowtalk.service.model.Response.SubscriberResponse;
import com.ayapa.yellowtalk.util.NetworkStatus;
import com.ayapa.yellowtalk.util.Valitation;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private final String PREFIX = "44";
    String inputvalue = null;
    String responseStatus = null;
    private EditText etPhone;
    private Button btnRegister;
    private SubscriberRequest mAccountRequest;
    private Subscription mSubscription;
    private Intent myIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        boolean isNetworkConnect = NetworkStatus.isInternetConnected(this);
        if (!isNetworkConnect) {
            NetworkStatus.popupDataWifiEnableMessage(this);
        } else {
        }

        etPhone = (EditText) findViewById(R.id.etPhone);

        etPhone.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        Drawable img = RegisterActivity.this.getResources().getDrawable(
                                R.drawable.gb);
                        img.setBounds(0, 0, 50, etPhone.getMeasuredHeight());
                        etPhone.setCompoundDrawables(img, null, null, null);
                        //etPhone.removeOnLayoutChangeListener(this);
                    }
                });

        etPhone.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int
                    dend) {
                return dstart < PREFIX.length() ? dest.subSequence(dstart, dend) : null;
            }
        }});
        etPhone.requestFocus();

        btnRegister = (Button) findViewById(R.id.btn_continue_reg);
        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_continue_reg:
                Log.i("Phone No >>", etPhone.getText().toString());
                if (etPhone.getText().toString().length() >= 10 && Valitation.validatePhoneNumber(etPhone.getText().toString())) {
                    // view subscriber for user is exist?
                    responseStatus = viewSubscriber();
                    if ("0".equalsIgnoreCase(responseStatus) || null == responseStatus) {
                        // popup message confirm number
                        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                                //.setTitle("Important")
                                //.setMessage("We will SMS a verification code to: +" + etPhone.getText().toString() + " \nIs this number correct?.")
                                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Call Code Verification Activity
                                        myIntent = new Intent(RegisterActivity.this, VerifyCodeActivity.class);
                                        myIntent.putExtra("phone_number", "+".concat(etPhone.getText().toString().trim()));
                                        startActivity(myIntent);
                                        //finish();
                                    }
                                })
                                .setNegativeButton("Edit Number", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        etPhone.requestFocus();
                                    }
                                });

                        TextView myTitle = new TextView(this);
                        myTitle.setText("Important");
                        myTitle.setTextSize(20);
                        myTitle.setTypeface(null, Typeface.BOLD);
                        myTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                        builder.setCustomTitle(myTitle);

                        TextView myMsg = new TextView(this);
                        myMsg.setText("We will SMS a verification code to: \n\b+" + etPhone.getText().toString() + "\b \n" + "Is this number correct?");
                        myMsg.setTextSize(16);
                        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
                        builder.setView(myMsg);

                        AlertDialog dialog = builder.create();
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

                        wmlp.gravity = Gravity.CENTER;
                        wmlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;

                        dialog.show();

                    } else if ("1".equalsIgnoreCase(responseStatus)) {
                        myIntent = new Intent(RegisterActivity.this, MainActivity.class);
                        startActivity(myIntent);
                    }

                } else {
                    // popup message allow notifications
                    AlertDialog.Builder builder = new AlertDialog.Builder(this)
                            //.setIcon(R.drawable.warning)
                            //.setMessage("Please enter valid phone number.")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    etPhone.requestFocus();
                                }
                            });

                    TextView myMsg = new TextView(this);
                    myMsg.setText("Please enter valid phone number.");
                    myMsg.setTextSize(16);
                    myMsg.setTextColor(getResources().getColor(R.color.colorAccent));
                    myMsg.setTypeface(null, Typeface.BOLD);
                    myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
                    builder.setView(myMsg);
                    //builder.setIcon(R.drawable.warning);

                    AlertDialog dialog = builder.create();
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

                    wmlp.gravity = Gravity.CENTER;
                    wmlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;

                    dialog.show();
                }
                break;
            default:
                break;
        }
    }

    private String viewSubscriber() {

        mAccountRequest = new SubscriberRequest();

        mAccountRequest.mobile = etPhone.getText().toString();
        mAccountRequest.username = "ayapa_api";
        mAccountRequest.userpwd = "top11_ayapa!rmapi";
        mAccountRequest.request_type = "view_subscriber";

        Log.i("AccountRequest >>", mAccountRequest.toString());

        Observable<SubscriberResponse> observable = YellowTalkService.getInstance().viewSubscriberMobile(mAccountRequest);

        this.mSubscription = observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<SubscriberResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.e("TAG", "On-Complete");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("TAG", "On-Error" + e.getMessage());
                    }

                    @Override
                    public void onNext(SubscriberResponse accountResponse) {
                        Log.e("viewSubscriber() >>", "On-Next");
                        /*Log.i("Response.acc_number >>", accountResponse.acc_number);
                        if(accountResponse.acc_number!=null) {
                            // save acc_number in shared preference
                            Preference.save(getApplicationContext(), PREFS_ACC_NO, accountResponse.acc_number);
                            Log.i("Preference AccountNo >>", Preference.getValue(getApplicationContext(), PREFS_ACC_NO));
                        }*/

                        if (accountResponse.request_status != null && "1".equalsIgnoreCase(accountResponse.request_status)) {
                            responseStatus = accountResponse.request_status;
                            Log.e("Response.request_status", responseStatus);
                            // User exist "1"

                            /*myIntent = new Intent(RegisterActivity.this, YellowTalkLoginActivity.class);
                            startActivity(myIntent);*/

                        } else if (accountResponse.request_status != null && "0".equalsIgnoreCase(accountResponse.request_status)) {
                            responseStatus = accountResponse.request_status;
                            Log.e("Response.request_status", responseStatus);
                            // User not exist "0"
                            // Call Register Activity for create account
                            /*myIntent = new Intent(RegisterActivity.this, VerifyCodeActivity.class);
                            startActivity(myIntent);*/
                        }
                    }
                });
        return responseStatus;
    }
}
