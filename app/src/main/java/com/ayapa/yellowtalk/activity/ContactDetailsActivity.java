package com.ayapa.yellowtalk.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.util.NetworkStatus;

public class ContactDetailsActivity extends AppCompatActivity {

    TextView selectedContactNumber;
    ImageView selectedPhoto;
    Button callButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        boolean isNetworkConnect = NetworkStatus.isInternetConnected(this);
        if (!isNetworkConnect) {
            NetworkStatus.popupDataWifiEnableMessage(this);
        } else {
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        selectedPhoto = (ImageView) findViewById(R.id.selectedPhoto);

        Bitmap bitmap = getIntent().getParcelableExtra("selected_photo");
        if (bitmap != null) {

            Bitmap bitmapRounded = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
            Canvas canvas = new Canvas(bitmapRounded);
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
            canvas.drawRoundRect((new RectF(0.0f, 0.0f, bitmap.getWidth(), bitmap.getHeight())), 20, 20, paint);

            selectedPhoto.setImageBitmap(bitmap);
        }

        selectedContactNumber = (TextView) findViewById(R.id.selectedContactNumber);
        selectedContactNumber.setText(getIntent().getExtras().getString("selected_name"));

        callButton = (Button) findViewById(R.id.btnCallNumber);
        callButton.setText(getIntent().getExtras().getString("selected_phone_number"));

        // add PhoneStateListener
        PhoneCallListener phoneListener = new PhoneCallListener();
        TelephonyManager telephonyManager = (TelephonyManager) this
                .getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phoneNumber = getIntent().getExtras().getString("selected_phone_number");
                Log.i("Clicked Item1 >> ", phoneNumber);
                Log.i("Clicked Prefix >> ", phoneNumber.substring(0,1));
                String takePhone = (phoneNumber.substring(0,1).equalsIgnoreCase("+") ? phoneNumber.substring(3) : phoneNumber);
                Log.i("Clicked Item number >> ", takePhone);
                String phone = "tel:+443309980120,," + takePhone;
                Log.i("Clicked Item2 >> ", phone);



                Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse(phone));
                startActivity(i);
            }
        });
    }

    //monitor phone call activities
    private class PhoneCallListener extends PhoneStateListener {

        String LOG_TAG = "LOGGING 123";
        private boolean isPhoneCalling = false;

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (TelephonyManager.CALL_STATE_RINGING == state) {
                // phone ringing
                Log.i(LOG_TAG, "RINGING, number: " + incomingNumber);
            }

            if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                // active
                Log.i(LOG_TAG, "OFFHOOK");

                isPhoneCalling = true;
            }

            if (TelephonyManager.CALL_STATE_IDLE == state) {
                // run when class initial and phone call ended,
                // need detect flag from CALL_STATE_OFFHOOK
                Log.i(LOG_TAG, "IDLE");

                if (isPhoneCalling) {

                    Log.i(LOG_TAG, "restart app");

                    // restart app
                    Intent i = getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage(
                                    getBaseContext().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);

                    isPhoneCalling = false;
                }

            }
        }
    }

}
