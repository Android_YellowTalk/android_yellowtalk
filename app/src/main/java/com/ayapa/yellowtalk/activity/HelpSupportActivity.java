package com.ayapa.yellowtalk.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.util.NetworkStatus;

public class HelpSupportActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout imageCall, imageSms, imageMail, imageDomain;
    TextView tvPhone, tvSms, tvMail, tvDomain;
    private Context context = this;
    boolean isNetworkConnect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_support);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        isNetworkConnect = NetworkStatus.isInternetConnected(this);
        if(!isNetworkConnect) {
            NetworkStatus.popupDataWifiEnableMessage(this);
        } else {
        }

        tvPhone = (TextView) findViewById(R.id.tvPhoneNo);
        tvSms = (TextView) findViewById(R.id.tvChatNo);
        tvMail = (TextView) findViewById(R.id.tvMailId);
        tvDomain = (TextView) findViewById(R.id.tvDomainName);

        imageCall = (RelativeLayout) findViewById(R.id.rlPhone);
        imageCall.setOnClickListener(this);
        imageSms = (RelativeLayout) findViewById(R.id.rlChat);
        imageSms.setOnClickListener(this);
        imageMail = (RelativeLayout) findViewById(R.id.rlMail);
        imageMail.setOnClickListener(this);
        imageDomain = (RelativeLayout) findViewById(R.id.rlDomain);
        imageDomain.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        RelativeLayout b = (RelativeLayout) v;
        switch (b.getId()) {
            case R.id.rlPhone:
                call(tvPhone.getText().toString().trim());
                break;
            case R.id.rlChat:
                sendSms(tvSms.getText().toString().trim());
                break;
            case R.id.rlMail:
                sendEmail(tvMail.getText().toString().trim());
                break;
            case R.id.rlDomain:
                isNetworkConnect = NetworkStatus.isInternetConnected(this);
                if(!isNetworkConnect) {
                    NetworkStatus.popupDataWifiEnableMessage(this);
                } else {
                    openDomain(tvDomain.getText().toString().trim());
                }
                break;
        }
    }

    private void call(String number) {
        Log.e("Number >>", number);
        number = number.replace("#", Uri.encode("#"));

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + number));
        intent.putExtra("incoming_number", number);
        intent.putExtra("yellowtalk_call", "yes");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        this.startActivity(intent);
    }

    private void sendSms(String number) {
        Uri uri = Uri.parse("smsto:" + number);

        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.putExtra("yellowtalk_sms", "yes");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        this.startActivity(intent);
    }

    private void sendEmail(String emailId) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{emailId});
        intent.setType("plain/text");

        this.startActivity(Intent.createChooser(intent, "Choose an Email client :"));
    }

    private void openDomain(String domainName) {
        //Intent intent = new Intent(Intent.ACTION_VIEW);
        //intent.setData(Uri.parse("http://".concat(domainName)));
        Intent intent = new Intent(HelpSupportActivity.this, HelpWebsiteActivity.class);
        intent.putExtra("domain_name", "http://".concat(domainName));
        this.startActivity(intent);
    }
}
