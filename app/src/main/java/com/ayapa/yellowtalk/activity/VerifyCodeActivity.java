package com.ayapa.yellowtalk.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.service.YellowTalkService;
import com.ayapa.yellowtalk.service.model.Request.AccountRequest;
import com.ayapa.yellowtalk.service.model.Response.AccountResponse;
import com.ayapa.yellowtalk.util.NetworkStatus;
import com.ayapa.yellowtalk.util.Preference;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
/*import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;*/
import com.google.firebase.auth.FirebaseUser;
import com.sinch.verification.CodeInterceptionException;
import com.sinch.verification.Config;
import com.sinch.verification.IncorrectCodeException;
import com.sinch.verification.InvalidInputException;
import com.sinch.verification.PhoneNumberUtils;
import com.sinch.verification.ServiceErrorException;
import com.sinch.verification.SinchVerification;
import com.sinch.verification.Verification;
import com.sinch.verification.VerificationListener;

import org.json.JSONObject;

import okhttp3.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class VerifyCodeActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String PREFS_ACC_NO = "ACCOUNT_NO";
    private static final String TAG = MainActivity.class.getSimpleName();
    String phoneString;
    private Intent myIntent;
    private View mProgressView;
    private TextView lblEnterVerifiCode, lblCountDown;
    private EditText etVerificationCode;
    private Button btnResendCode, btnConfirmCode;
    private Verification verification;
    private AccountRequest mAccountRequest;
    private Subscription mSubscription;
    /*private Firebase mFirebaseRef;
    private FirebaseAuth mAuth;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MultiDex.install(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_code);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        boolean isNetworkConnect = NetworkStatus.isInternetConnected(this);
        if (!isNetworkConnect) {
            NetworkStatus.popupDataWifiEnableMessage(this);
        } else {
        }

        // Call account creation request
        createAccount();

        /*Firebase.setAndroidContext(this);
        mAuth = FirebaseAuth.getInstance();
        // Create the Firebase ref that is used for all authentication with Firebase
        //mFirebaseRef = new Firebase("https://yellow-talk-82779.firebaseio.com");

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        mAuth.addAuthStateListener(mAuthListener);*/

        //String phoneString = "+447454644877";

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                phoneString = null;
            } else {
                phoneString = extras.getString("phone_number");
            }
        } else {
            phoneString = (String) savedInstanceState.getSerializable("phone_number");
        }

        // Sinch verification request
        // Create a configuration using Ayapa application key
        Config config = SinchVerification.config().applicationKey("5ed9928a-36c4-4d3b-a012-a4cc1d05cb79").context(getApplicationContext()).build();
        String defaultRegion = PhoneNumberUtils.getDefaultCountryIso(VerifyCodeActivity.this);
        String phoneNumberInE164 = PhoneNumberUtils.formatNumberToE164(phoneString, defaultRegion);
        verification = SinchVerification.createSmsVerification(config, phoneNumberInE164, listener);
        verification.initiate();

        lblEnterVerifiCode = (TextView) findViewById(R.id.lblEnterVerifiCode);
        lblEnterVerifiCode.setText(getResources().getString(R.string.lbl_enter_verifi_code).concat(" " + phoneString));

        etVerificationCode = (EditText) findViewById(R.id.input_verification_code);

        btnResendCode = (Button) findViewById(R.id.btn_resend_code);
        btnResendCode.setOnClickListener(this);
        btnResendCode.setEnabled(false);

        btnConfirmCode = (Button) findViewById(R.id.btn_confirm_code);
        btnConfirmCode.setOnClickListener(this);

        displayCountDown();

        mProgressView = findViewById(R.id.verify_progress);

    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    VerificationListener listener = new VerificationListener() {
        @Override
        public void onInitiated() {
            // Verification initiated
        }

        @Override
        public void onInitiationFailed(Exception e) {
            String messageTitle = "";
            if (e instanceof InvalidInputException) {
                // Incorrect number provided
                messageTitle = "Failed, Invalid Input";
            } else if (e instanceof ServiceErrorException) {
                // Sinch service error
                messageTitle = "Sinch Service Failed";
            } else {
                // Other system error, such as UnknownHostException in case of network error
                messageTitle = "Sinch Verification Failed";
             }
            /*AlertDialog.Builder builder = new AlertDialog.Builder(VerifyCodeActivity.this)
                    //.setIcon(R.drawable.warning)
                    //.setTitle(messageTitle)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            etVerificationCode.requestFocus();
                        }
                    });

            TextView myTitle = new TextView(VerifyCodeActivity.this);
            myTitle.setText(messageTitle);
            myTitle.setTextSize(20);
            myTitle.setGravity(Gravity.CENTER_HORIZONTAL);
            builder.setCustomTitle(myTitle);
            builder.setIcon(R.drawable.warning);

            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

            wmlp.gravity = Gravity.CENTER;
            wmlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;

            dialog.show();*/

        }

        @Override
        public void onVerified() {

            showProgress(false);
            Preference.savePreference(getString(R.string.sharedPrefLoginStatus), "LOGIN", VerifyCodeActivity.this);
            // Call Register Activity
            myIntent = new Intent(VerifyCodeActivity.this, MainActivity.class);
            startActivity(myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            finish();

            // Verification successful
            /*AlertDialog.Builder builder = new AlertDialog.Builder(VerifyCodeActivity.this)
                    //.setIcon(R.drawable.success)
                    //.setTitle("Verification success.")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Preference.savePreference(getString(R.string.sharedPrefLoginStatus), "LOGIN", VerifyCodeActivity.this);
                            // Call Register Activity
                            myIntent = new Intent(VerifyCodeActivity.this, MainActivity.class);
                            startActivity(myIntent);
                            finish();

                            dialog.dismiss();
                        }
                    });

            TextView myTitle = new TextView(VerifyCodeActivity.this);
            myTitle.setText("Verification success.");
            myTitle.setTextSize(18);
            myTitle.setTypeface(null, Typeface.BOLD);
            myTitle.setGravity(Gravity.CENTER_HORIZONTAL);
            builder.setCustomTitle(myTitle);
            builder.setIcon(R.drawable.success);

            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

            wmlp.gravity = Gravity.CENTER;
            wmlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;

            dialog.show();*/

        }

        @Override
        public void onVerificationFailed(Exception e) {
            showProgress(false);
            String messageTitle = "";
            if (e instanceof InvalidInputException) {
                // Incorrect number or code provided
                messageTitle = "Sinch Verification Failed";
            } else if (e instanceof CodeInterceptionException) {
                // Intercepting the verification code automatically failed, input the code manually with verify()
                messageTitle = "Sinch Verification Failed";
            } else if (e instanceof IncorrectCodeException) {
                // The verification code provided was incorrect
                messageTitle = "Sinch Verification Failed";
            } else if (e instanceof ServiceErrorException) {
                // Sinch service error
                messageTitle = "Sinch Verification Failed";
            } else {
                // Other system error, such as UnknownHostException in case of network error
                messageTitle = "Sinch Verification Failed";
            }

            /*AlertDialog.Builder builder = new AlertDialog.Builder(VerifyCodeActivity.this)
                    //.setIcon(R.drawable.warning)
                    //.setTitle(messageTitle)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            etVerificationCode.requestFocus();
                        }
                    });

            TextView myTitle = new TextView(VerifyCodeActivity.this);
            myTitle.setText(messageTitle);
            myTitle.setTextSize(20);
            myTitle.setTextColor(getResources().getColor(R.color.colorAccent));
            myTitle.setTypeface(null, Typeface.BOLD);
            myTitle.setGravity(Gravity.CENTER_HORIZONTAL);
            builder.setCustomTitle(myTitle);
            builder.setIcon(R.drawable.warning);

            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

            wmlp.gravity = Gravity.CENTER;
            wmlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;

            dialog.show();*/

            btnConfirmCode.setEnabled(true);
            btnConfirmCode.setBackgroundResource(R.drawable.button_shape);
        }
    };
    //private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_confirm_code:
                if (etVerificationCode.getText().toString().length() == 4) {
                    showProgress(true);
                    btnConfirmCode.setBackgroundResource(R.drawable.button_shape_disabled);
                    btnConfirmCode.setEnabled(false);
                    // Verify user input code with Sinch generated code
                    verification.verify(etVerificationCode.getText().toString());

                } else {
                    // popup message allow notifications
                    AlertDialog.Builder builder = new AlertDialog.Builder(VerifyCodeActivity.this)
                            //.setIcon(R.drawable.warning)
                            //.setTitle("Please enter 4 digits code.")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    etVerificationCode.requestFocus();
                                }
                            });
                    TextView myTitle = new TextView(this);
                    myTitle.setText("Please enter 4 digits code.");
                    myTitle.setTextSize(20);
                    myTitle.setTextColor(getResources().getColor(R.color.colorAccent));
                    myTitle.setTypeface(null, Typeface.BOLD);
                    myTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                    builder.setCustomTitle(myTitle);
                    builder.setIcon(R.drawable.warning);

                    AlertDialog dialog = builder.create();
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

                    wmlp.gravity = Gravity.CENTER;
                    wmlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;

                    dialog.show();
                }
                break;
            case R.id.btn_resend_code:

                verification.initiate();
                btnConfirmCode.setEnabled(true);
                btnConfirmCode.setBackgroundResource(R.drawable.button_shape);
                btnResendCode.setBackgroundResource(R.drawable.button_shape_disabled);

                displayCountDown();

                break;
            default:
                break;
        }
    }

    public void displayCountDown() {
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                btnConfirmCode.setBackgroundResource(R.drawable.button_shape);
                btnResendCode.setBackgroundResource(R.drawable.button_shape_disabled);
                btnConfirmCode.setEnabled(true);
                btnResendCode.setText("seconds : " + millisUntilFinished / 1000);
                btnResendCode.setEnabled(false);
            }

            public void onFinish() {
                //btnConfirmCode.setBackgroundResource(R.drawable.button_shape_disabled);
                btnConfirmCode.setBackgroundResource(R.drawable.button_shape);
                btnResendCode.setBackgroundResource(R.drawable.button_shape);
                btnConfirmCode.setEnabled(true);
                btnResendCode.setText("Resend");
                btnResendCode.setEnabled(true);
            }
        }.start();
    }

    private void createAccount() {

        mAccountRequest = new AccountRequest();
        mAccountRequest.county = "Sri Lanka";
        mAccountRequest.title = "Mr";
        mAccountRequest.acc_password = "123";
        mAccountRequest.email = "test@gmail.com";
        mAccountRequest.addr1 = "No 34";
        mAccountRequest.addr2 = "Kandy Road";
        mAccountRequest.addr3 = "970006";
        mAccountRequest.mobile = phoneString;
        mAccountRequest.postcode = "090";
        mAccountRequest.username = "ayapa_api";
        mAccountRequest.userpwd = "top11_ayapa!rmapi";
        mAccountRequest.request_type = "account_reg";

        Log.i("AccountRequest >>", mAccountRequest.toString());

        try {
            Observable<AccountResponse> observable = YellowTalkService.getInstance().createAccount(mAccountRequest);

            this.mSubscription = observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<AccountResponse>() {
                        @Override
                        public void onCompleted() {
                            Log.e("TAG", "On-Complete");
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e("TAG", "On-Error" + e.getMessage());
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(AccountResponse accountResponse) {
                            Log.e("TAG", "On-Next");

                            if (accountResponse.acc_number != null) {
                                Log.i("Response.acc_number >>", accountResponse.acc_number);
                                Preference.savePreference(getString(R.string.sharedPrefAccountNo), accountResponse.acc_number, VerifyCodeActivity.this);
                            } else {
                                Log.i("Response.acc_number >>", "NULL");
                            }
                            // create firebase account for chat
                            //createFirebaseAccount(phoneString + "@yellow-talk-82779.firebaseapp.com", "123456");

                            Preference.savePreference(getString(R.string.sharedPrefMyMobileNo), phoneString, VerifyCodeActivity.this);
                        }
                    });
        } catch (Exception e){
            Log.e("createAccount >>>", e.getMessage());
            e.printStackTrace();
        }

    }

    /*@Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void createFirebaseAccount(String email, String password) {

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.e(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            Log.e(TAG, "createUserWithEmail:failed", task.getException());
                            Toast.makeText(VerifyCodeActivity.this, "Failed user creation",
                                    Toast.LENGTH_LONG).show();
                            //showProgress(false);
                        } else {
                            Log.e(TAG, "createUserWithEmail:success", null);
                            Toast.makeText(VerifyCodeActivity.this, "Successfully user created",
                                    Toast.LENGTH_LONG).show();
                            //showProgress(false);
                        }
                    }
                });
    }*/

    @Override
    protected void onDestroy() {
        this.mSubscription.unsubscribe();
        showProgress(false);
        super.onDestroy();
    }
}
