package com.ayapa.yellowtalk.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.model.CountryRate;

import java.util.ArrayList;

/**
 * Created by Thanjeedth on 8/15/2016.
 */
public class CountryRateAdapter extends ArrayAdapter<CountryRate> {
    public CountryRateAdapter(Context context, ArrayList<CountryRate> rates) {
        super(context, 0, rates);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        CountryRate rate = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.rate_list_item, parent, false);
        }
        // Lookup view for data population
        ImageView ivFlag = (ImageView) convertView.findViewById(R.id.ivFlag);
        TextView tvName = (TextView) convertView.findViewById(R.id.tvCountry);
        TextView tvCode = (TextView) convertView.findViewById(R.id.tvCode);
        // Populate the data into the template view using the data object

        Resources res = convertView.getResources();
        int resourceId = res.getIdentifier(
                rate.isoCode, "drawable", null);

        ivFlag.setImageResource(resourceId);
        tvName.setText(rate.name);
        tvCode.setText(rate.code);
        // Return the completed view to render on screen
        return convertView;
    }

}
