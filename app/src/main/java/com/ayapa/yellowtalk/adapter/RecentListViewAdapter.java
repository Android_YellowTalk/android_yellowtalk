package com.ayapa.yellowtalk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.model.CallLogBean;
import com.ayapa.yellowtalk.model.CountryRate;

import java.util.ArrayList;

public class RecentListViewAdapter extends BaseAdapter {

    // Declare Variables
    Context context;
    String[] phone, date, duration;

    LayoutInflater inflater;

    public RecentListViewAdapter(Context context, String number[], String date[], String duration[]) {
        this.context = context;

        this.phone = number;
        this.date = date;
        this.duration = duration;

        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if(phone!=null)
            return phone.length;
        else
            return 0;
    }

    @Override
    public Object getItem(int position) {

        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder
    {
        TextView txtPhone;
        TextView txtDate;
        TextView txtDuration;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if(convertView==null)
        {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.recent_list_item, null);

            holder.txtPhone = (TextView) convertView.findViewById(R.id.tvPhoneNo);
            holder.txtDate = (TextView) convertView.findViewById(R.id.tvCallDate);
            holder.txtDuration = (TextView) convertView.findViewById(R.id.tvCallDuration);

            convertView.setTag(holder);
        }
        else
            holder=(ViewHolder)convertView.getTag();

        holder.txtPhone.setText(phone[position]);
        holder.txtDate.setText(date[position]);
        holder.txtDuration.setText(duration[position]);

        return convertView;
    }
}