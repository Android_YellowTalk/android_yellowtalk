package com.ayapa.yellowtalk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.model.CountryRate;

import java.util.ArrayList;

public class ListViewAdapter extends BaseAdapter implements Filterable {

    // Declare Variables
    Context context;
    //    String[] country;
//    String[] code;
//    int[] flag;
    LayoutInflater inflater;
    ArrayList<CountryRate> countryList;
    ArrayList<CountryRate> originalData;

    public ListViewAdapter(Context context, ArrayList<CountryRate> countrys) {
        this.context = context;
//        this.country = country;
//        this.code = code;
//        this.flag = flag;
        countryList = countrys;
        originalData = countrys;
    }

    @Override
    public int getCount() {
        return countryList.size();
    }

    @Override
    public Object getItem(int position) {

        return countryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        // Declare Variables
        TextView txtcountry;
        TextView txtcode;
        ImageView imgflag;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.rate_list_item, parent, false);

        // Locate the TextViews in listview_item.xml
        txtcountry = (TextView) itemView.findViewById(R.id.tvCountry);
        txtcode = (TextView) itemView.findViewById(R.id.tvCode);
        // Locate the ImageView in listview_item.xml
        imgflag = (ImageView) itemView.findViewById(R.id.ivFlag);

        // Capture position and set to the TextViews
        txtcountry.setText(countryList.get(position).name);
        txtcode.setText(countryList.get(position).code);

        //flag[i] = resourceId;

        // Capture position and set to the ImageView

        ///if (countryList.get(position).imageID > 0)
        imgflag.setImageResource(countryList.get(position).imageID);

        return itemView;
    }

    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String filterString = constraint.toString().toLowerCase();

                // constraint = constraint.toString().toLowerCase();
                FilterResults result = new FilterResults();

                if (constraint != null && constraint.toString().length() > 0) {
                    final ArrayList<CountryRate> list = originalData;

                    int count = list.size();
                    final ArrayList<CountryRate> nlist = new ArrayList<CountryRate>(count);

                    CountryRate filterableString;

                    for (int i = 0; i < count; i++) {
                        filterableString = list.get(i);
                        String countryname = filterableString.name;
                        if (countryname.toLowerCase().startsWith(filterString)) {
                            nlist.add(filterableString);
                        }
                    }

                    result.values = nlist;
                    result.count = nlist.size();
                } else {
                    result.values = originalData;
                    result.count = originalData.size();

                }
                return result;


            }

            @Override
            protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
                final ArrayList<CountryRate> filteredDate = (ArrayList<CountryRate>) results.values;
                countryList = filteredDate;
                //new ListViewAdapter(context, filteredDate);
                notifyDataSetChanged();
            }

        };
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
