package com.ayapa.yellowtalk.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ayapa.yellowtalk.R;
import com.ayapa.yellowtalk.model.ContactBean;

public class T9Adapter extends BaseAdapter implements Filterable {

	private LayoutInflater mInflater;
	private List<ContactBean> list;
	private List<ContactBean> allContactList;
	private Context context;
	private String filterNum;
	private Intent intent = new Intent("com.adouming.refreshcalllog.RECEIVER");
	public T9Adapter(Context context) {     
		mInflater = LayoutInflater.from(context); 
		this.list = new ArrayList<ContactBean>();
		this.context=context;
	}   

	public void assignment(List<ContactBean> list){
		this.allContactList = list;
		this.list = this.allContactList;
	}
	public void add(ContactBean bean) {
		list.add(bean);
	}
	public void remove(int position){
		list.remove(position);
	}
	public int getCount() {  
		return list != null ? list.size() : -1;     
	}            
	public ContactBean getItem(int position) {    
		return list.get(position);     
	}          
	public long getItemId(int position) {
		return 0;   
	}           
	 
	 public static String implode(String separator, String[] data) {
	     StringBuilder sb = new StringBuilder();
	     for (int i = 0; i < data.length - 1; i++) {
	     //data.length - 1 => to not add separator at the end
	         if (!data[i].matches(" *")) {//empty string are ""; " "; "  "; and so on
	             sb.append(data[i]);
	             sb.append(separator);
	         }
	     }
	     sb.append(data[data.length - 1]);
	     return sb.toString();
	 }
	public View getView(int position, View convertView, ViewGroup parent) {   

		ViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.contact_list_item, parent, false);
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.contact_name);

			holder.number = (TextView) convertView.findViewById(R.id.phone_number);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}

		holder.name.setText(list.get(position).getName());
		convertView.setTag(holder);
		ContactBean cb = (ContactBean)list.get(position);

		return convertView;
	}   

	public final class ViewHolder {   
		public TextView name;        
		public TextView pinyin;        
		public TextView number;        
	}

	public char[] digit2Char(int digit) {
		char[] cs = null;
		switch (digit) {
		case 0:
			cs = new char[] {};
			break;
		case 1:
			break;
		case 2:
			cs = new char[] { 'a', 'b', 'c' };
			break;
		case 3:
			cs = new char[] { 'd', 'e', 'f' };
			break;
		case 4:
			cs = new char[] { 'g', 'h', 'i' };
			break;
		case 5:
			cs = new char[] { 'j', 'k', 'l' };
			break;
		case 6:
			cs = new char[] { 'm', 'n', 'o' };
			break;
		case 7:
			cs = new char[] { 'p', 'q', 'r', 's' };
			break;
		case 8:
			cs = new char[] { 't', 'u', 'v' };
			break;
		case 9:
			cs = new char[] { 'w', 'x', 'y', 'z' };
			break;
		}
		return cs;
	}

	public Filter getFilter() {
		Filter filter = new Filter() {
			protected void publishResults(CharSequence constraint, FilterResults results) {
				list = (ArrayList<ContactBean>) results.values;
				if (results.count > 0) {
					notifyDataSetChanged();
				} else {
					notifyDataSetInvalidated();
				}
			}
			protected FilterResults performFiltering(CharSequence s) {
				String str = s.toString();
				filterNum = str;
				FilterResults results = new FilterResults();
				ArrayList<ContactBean> contactList = new ArrayList<ContactBean>();

				results.values = contactList;
				results.count = contactList.size();
				return results;
			}
		};
		return filter;
	}
}
