package com.ayapa.yellowtalk.chat;

/**
 * Created by Thanjeedth on 8/30/2016.
 */
public class Chat {
    private String message;
    private String author;

    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    private Chat() {
    }

    public Chat(String message, String author) {
        this.message = message;
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public String getAuthor() {
        return author;
    }
}
