package com.ayapa.yellowtalk.service;

import com.ayapa.yellowtalk.service.model.Request.AccountRequest;
import com.ayapa.yellowtalk.service.model.Request.SubscriberRequest;
import com.ayapa.yellowtalk.service.model.Request.UserRequest;
import com.ayapa.yellowtalk.service.model.Response.AccountResponse;
import com.ayapa.yellowtalk.service.model.Response.SubscriberResponse;
import com.ayapa.yellowtalk.service.model.Response.UserResponse;

import org.json.JSONObject;

import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Thanjeedth on 7/23/2016.
 */
public interface YellowTalkApi {

    @Headers({
            "Content-Type: text/xml",
            "Accept-Charset: utf-8"
    })

    @POST("/save_account.asp?")
    Observable<AccountResponse> createAccount(@Body AccountRequest accountRequest);

    /*@POST("/save_account.asp?")
    Observable<AccountResponse> createAccount(@Body JSONObject accountRequest);*/

    @POST("/view_subscriber_mobile.asp?")
    Observable<SubscriberResponse> viewSubscriberMobile(@Body SubscriberRequest accountRequest);

    @POST("/save_user.asp?")
    Observable<UserResponse> saveUser(@Body UserRequest accountRequest);
}
