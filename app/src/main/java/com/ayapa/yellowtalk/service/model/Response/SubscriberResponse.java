package com.ayapa.yellowtalk.service.model.Response;

/**
 * Created by Thanjeedth on 9/10/2016.
 */
public class SubscriberResponse {
    public String request_status;
    public String transaction_id;
    public String subscriber_timestamp;
    public String subscriber_authuser;
    public String subscriber_authpwd;
    public String subscriber_mobile;
    public String error_msg;
}
