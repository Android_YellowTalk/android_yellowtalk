package com.ayapa.yellowtalk.service.model.Request;

import android.text.format.DateFormat;

/**
 * Created by Thanjeedth on 7/23/2016.
 */
public class AccountRequest {
    public String timestamp = (DateFormat.format("dd-MM-yyyy hh:mm:ss", new java.util.Date()).toString());
    public String reseller;
    public String title;
    public String fname;
    public String lname;
    public String addr1;
    public String addr2;
    public String addr3;
    public String town;
    public String county;
    public String postcode;
    public String acc_password;
    public String hometel;
    public String mobile;
    public String fax;
    public String worktel;
    public String email;
    public String request_type;
    public String username;
    public String userpwd;
}
