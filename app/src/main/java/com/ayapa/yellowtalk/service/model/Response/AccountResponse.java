package com.ayapa.yellowtalk.service.model.Response;

/**
 * Created by Thanjeedth on 7/23/2016.
 */
public class AccountResponse {

    public String request_status;
    public String transaction_id;
    public String acc_timestamp;
    public String acc_template;
    public String acc_authuser;
    public String acc_authpwd;
    public String acc_reseller;
    public String acc_title;
    public String acc_fname;
    public String acc_lname;
    public String acc_occupation;
    public String acc_dob;
    public String acc_cob;
    public String acc_addr1;
    public String acc_addr2;
    public String acc_addr3;
    public String acc_addr4;
    public String acc_addr5;
    public String acc_postcode;
    public String acc_country;
    public String acc_pwd;
    public String acc_hometel;
    public String acc_mobile;
    public String acc_fax;
    public String acc_heardfrom;
    public String acc_worktel;
    public String acc_email;
    public String acc_number;
    public String error_msg;

}
