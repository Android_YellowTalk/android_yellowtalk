package com.ayapa.yellowtalk.service.model.Response;

/**
 * Created by Thanjeedth on 9/10/2016.
 */
public class UserResponse {
    public String request_status;
    public String transaction_id;
    public String user_timestamp;
    public String user_template;
    public String user_authuser;
    public String user_authpwd;
    public String user_tel;
    public String user_callback;
    public String user_desc;
    public String user_serial;
    public String error_msg;
}
