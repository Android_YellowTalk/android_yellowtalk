package com.ayapa.yellowtalk.service.model.Request;

import android.text.format.DateFormat;

/**
 * Created by Thanjeedth on 9/10/2016.
 */
public class UserRequest {
    public String timestamp = (DateFormat.format("dd-MM-yyyy hh:mm:ss", new java.util.Date()).toString());
    public String accno;
    public String tel;
    public String desc;
    public String request_type;
    public String username;
    public String userpwd;
}
