package com.ayapa.yellowtalk.service;

import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.ayapa.yellowtalk.YellowTalkConstant;
import com.ayapa.yellowtalk.service.model.Request.AccountRequest;
import com.ayapa.yellowtalk.service.model.Request.SubscriberRequest;
import com.ayapa.yellowtalk.service.model.Request.UserRequest;
import com.ayapa.yellowtalk.service.model.Response.AccountResponse;
import com.ayapa.yellowtalk.service.model.Response.SubscriberResponse;
import com.ayapa.yellowtalk.service.model.Response.UserResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import rx.Observable;

/**
 * Created by Thanjeedth on 7/23/2016.
 */
public class YellowTalkService {

    private static YellowTalkService sYellowTalkService;
    private YellowTalkApi mYellowTalkApi;

    private YellowTalkService() {
        /*HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        Strategy strategy = new AnnotationStrategy();
        Serializer serializer = new Persister(strategy);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .build();

        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.create();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(SimpleXmlConverterFactory.create(serializer))
                .addCallAdapterFactory(rxAdapter)
                .baseUrl(YellowTalkConstant.BASE_URL)
                .client(okHttpClient)
                .build();*/

        /*Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder().addHeader("User-Agent", "Retrofit-Sample-App").build();
                return chain.proceed(newRequest);
            }
        };
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        OkHttpClient client = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(YellowTalkConstant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();*/

        Strategy strategy = new AnnotationStrategy();

        // Define the interceptor, add authentication headers
        Interceptor interceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder().addHeader("User-Agent", "Retrofit-Sample-App").build();
                return chain.proceed(newRequest);
            }
        };


        Serializer serializer = new Persister(strategy);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        OkHttpClient client = builder.build();

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();

        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.networkInterceptors().add(httpLoggingInterceptor);
        builder.build();

        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.create();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(SimpleXmlConverterFactory.create(serializer))
                .addCallAdapterFactory(rxAdapter)
                .baseUrl(YellowTalkConstant.BASE_URL)
                .client(client)
                .build();

        try{
            this.mYellowTalkApi = retrofit.create(YellowTalkApi.class);
        }catch (Exception ex) {
            Log.e("Exception", ex.toString());
        }

        //this.mYellowTalkApi = retrofit.create(YellowTalkApi.class);
    }

    public static YellowTalkService getInstance() {
        if (sYellowTalkService == null) {
            sYellowTalkService = new YellowTalkService();
        }
        return sYellowTalkService;
    }

    public Observable<AccountResponse> createAccount(AccountRequest accountRequest) {
        return this.mYellowTalkApi.createAccount(accountRequest);
    }

    public Observable<SubscriberResponse> viewSubscriberMobile(SubscriberRequest accountRequest) {
        return this.mYellowTalkApi.viewSubscriberMobile(accountRequest);
    }

    public Observable<UserResponse> saveUser(UserRequest accountRequest) {
        return this.mYellowTalkApi.saveUser(accountRequest);
    }
}
