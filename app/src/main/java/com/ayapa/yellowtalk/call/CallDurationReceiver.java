package com.ayapa.yellowtalk.call;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.format.Time;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Thanjeedth on 9/3/2016.
 */
public class CallDurationReceiver extends BroadcastReceiver {
    // instance variables of sharedpreferences
    SharedPreferences mSharedPrefernce;
    SharedPreferences.Editor e;

    // String variables for number,date,time,calltype
    String number, date, time, calltype;
    long startTime, endTime;

    @Override
    public void onReceive(final Context context, Intent intent) {

        Log.v("info", "calls info....");

        // initialising the sharedpreferences
        mSharedPrefernce = context.getSharedPreferences("MyPref", 0);
        e = mSharedPrefernce.edit();

        Bundle bundle = intent.getExtras();
        if (bundle == null)
            return;

        // initialising the variables
        number = null;
        startTime = 0;
        endTime = 0;

        // getting incoming call details
        String state = bundle.getString(TelephonyManager.EXTRA_STATE);
        if ((state != null)
                && (state
                .equalsIgnoreCase(TelephonyManager.EXTRA_STATE_RINGING))) {

            Log.v("info", "Phone Ringing..");

            number = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            Log.v("info", "Incomng Number: " + number);

            calltype = "Incoming";

            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();

            date = today.monthDay + "-" + (today.month + 1) + "-" + today.year;
            time = today.format("%k:%M:%S");

            // putting the values into the SharedPreferences
            e.putString("number", number);
            e.putString("Type", calltype);
            e.putString("date", date);
            e.putString("time", time);
            e.commit();

            Toast.makeText(
                    context,
                    "Detect Calls sample application\nIncoming number: "
                            + number, Toast.LENGTH_SHORT).show();

        }
        // getting outgoing call details
        else if (state == null) {
            number = bundle.getString(Intent.EXTRA_PHONE_NUMBER);
            Log.v("info", "Outgoing Number: " + number);

            calltype = "Outgoing";

            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();

            date = today.monthDay + "-" + (today.month + 1) + "-" + today.year;
            time = today.format("%k:%M:%S");

            // putting the values into the SharedPreferences
            e.putString("number", number);
            e.putString("Type", calltype);
            e.putString("date", date);
            e.putString("time", time);
            e.commit();

            Toast.makeText(
                    context,
                    "Detect Calls sample application\nOutgoing number: "
                            + number, Toast.LENGTH_SHORT).show();

        }
        // called when the call is answered
        else if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
            Log.v("info", "Call Ansered..");

            startTime = System.currentTimeMillis();
            e.putLong("start", startTime);
            e.commit();

        }
        // called when the call is ended
        else if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_IDLE)) {
            Log.v("info", "Call Ended..");

            String phonenumber=null, type=null, date1=null, time1=null,  duration=null;

            // getting the values from the SharedPreferences
            phonenumber = mSharedPrefernce.getString("number", "");
            type = mSharedPrefernce.getString("Type", "");
            date1 = mSharedPrefernce.getString("date", "");
            time1 = mSharedPrefernce.getString("time", "");
            long start=0;
            start = mSharedPrefernce.getLong("start", 0);
            Log.v("info", "startTime=" + start);

            // clearing the SharedPreferences
            mSharedPrefernce.edit().clear();

            endTime = System.currentTimeMillis();
            Log.v("info", "endTime=" + endTime);
            long totalTime =0;
            totalTime = endTime - start;

            DateFormat df = new SimpleDateFormat("HH':'mm':'ss");
            df.setTimeZone(TimeZone.getTimeZone("GMT+0"));

            duration = df.format(new Date(totalTime));

            System.out.println("GOT SOMETHING - "+phonenumber + " " + date1 + " " + time1 + " " + duration + " " + type);

            Toast.makeText(context, phonenumber + " " + date1 + " " + time1 + " " + duration + " " + type, Toast.LENGTH_LONG).show();


        }

    }
}
