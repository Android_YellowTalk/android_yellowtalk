package com.ayapa.yellowtalk.call;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.HandlerThread;
import android.os.IBinder;
import android.widget.Toast;

/**
 * Created by Thanjeedth on 9/3/2016.
 */
public class YellowTalkCallService extends Service {
    CallDurationReceiver receiver = null;


    @Override
    public void onCreate() {

        receiver = new CallDurationReceiver();

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        filter.addAction(android.telephony.TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        registerReceiver(receiver, filter);

        HandlerThread thread = new HandlerThread("ServiceStartArguments");
        thread.start();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(receiver);
        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
    }
}
